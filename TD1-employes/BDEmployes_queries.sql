-- Q1-Donner les différents jobs des employés.
SELECT DISTINCT job FROM EMP;

-- Q2-Donner le numéro des projets auxquels participe l'employé numéro 7900 avec le nombre d’heures.
SELECT PNO, HOURS FROM PARTICIPATION
WHERE EMPNO=7900;

-- Q3-Donner le nom et la catégorie des projets avec des employés.
SELECT DISTINCT PNAME, CAT FROM PROJECT PR 
INNER JOIN PARTICIPATION P ON PR.PNO=P.PNO;

-- Q4-Donner le numéro des projets auxquels participent ADAMS ou MARTIN.

-- Q5-Donner le nom des départements avec des clerk qui travaillent sur le projet 2.

-- Q6-Donner le nom des employés dirigés directement par KING.

-- Q7-Donner le numéro des employés embauchés avant leur manager.

-- Q8-Donner la requête SQL correspondant à la requête algébrique.

-- Q9-Donner le numéro et le nom de tous les départements avec leurs jobs.

-- Q10-Donner le numéro des départements qui n'ont pas d'employé.

-- Q11-Donner le nom des projets auxquels participent l'employé numéro 7900 et l'employé numéro 7521.

-- Q12-Donner le numéro des projets de la catégorie A ou mobilisant l'employé numéro 7876.

-- Q13-Donner le nombre de jobs différents.

-- Q14-Donner le salaire moyen et la commission moyenne pour chaque job.

-- Q15-Donner le numéro des projets avec au moins 4 participants.

-- Q16-Donner le nom des employés dont le nom commence par A et participant à au moins 2 projets.

-- Q17-Donner le nombre de projets par employés (afficher leur numéro et leur nom).

-- Q18-Donner le nombre d'employés moyen par job.

-- Q19-Donner le job ayant le salaire moyen le plus faible.

-- Q20-Donner le numéro des employés qui participent à tous les projets.

-- Q21-Donner le numéro des employés qui participent à tous les projets de la catégorie C.

-- Q22-Donner le nom des projets de la catégorie B qui mobilisent tous les employés.

-- Q23-Donner le nom des employés qui dépendent (directement ou non) de JONES.
SELECT ENAME, LEVEL FROM EMP 
CONNECT BY PRIOR EMPNO = MGR 
START WITH MGR = (SELECT EMPNO FROM EMP WHERE ENAME='JONES');

-- Q24-Donner le nom des employés dirigés directement par KING.
-- Requête Q6 mais en utilisant CONNECT BY et LEVEL.
SELECT ENAME FROM EMP 
WHERE LEVEL = 1
CONNECT BY PRIOR EMPNO = MGR 
START WITH MGR = (SELECT EMPNO FROM EMP WHERE ENAME='JONES');

-- Q25-Donner le nom des supérieurs de SMITH.
SELECT ENAME, LEVEL FROM EMP  
CONNECT BY PRIOR MGR = EMPNO 
START WITH EMPNO = (SELECT MGR FROM EMP WHERE ENAME='SMITH');

-- Q26-Donner le numéro, le nom et la date d'embauche (formatée en DD/MM)
-- des employés embauchés entre le 01/04/1981 et le 30/09/1981
-- par ordre décroissant de la date puis nom croissant.
SELECT EMPNO, ENAME, HIREDATE 
FROM EMP
WHERE HIREDATE > '01/04/1981' AND HIREDATE < '30/09/1981'
ORDER BY HIREDATE DESC, ENAME ASC;

-- Q27-Donner le numéro, le nom et le revenu total des employés embauchés un 13/05
-- par ordre croissant de la date puis revenu décroissant.
SELECT EMPNO, ENAME, HIREDATE, SAL+NVL(COMM, 0) AS REVENU FROM EMP
ORDER BY HIREDATE ASC, REVENU DESC;